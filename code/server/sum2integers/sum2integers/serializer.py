from rest_framework import serializers

from sum2integers.models import Sum


class SumSerializer(serializers.ModelSerializer):
    class Meta:
        model = Sum
        fields = ['id', 'sum', 'creation_date']
