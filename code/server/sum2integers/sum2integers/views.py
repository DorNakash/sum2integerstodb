import random
from .models import Sum
from rest_framework import status
from rest_framework.decorators import api_view, renderer_classes
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from .serializer import SumSerializer


@api_view(['GET'])
@renderer_classes([JSONRenderer])
def sum_two_integers(request):
    first_num = int(request.GET.get('firstNumber', ''))
    second_num = int(request.GET.get('secondNumber', ''))
    print(first_num)
    print(second_num)
    calculation = first_num + second_num
    sum_obj = Sum(sum=calculation)
    sum_obj.save()
    return Response(SumSerializer(sum_obj).data, status=status.HTTP_200_OK)



