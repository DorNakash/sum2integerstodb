from django.db import models


class Sum(models.Model):
    id = models.AutoField(primary_key=True)
    sum = models.IntegerField(unique=True)
    creation_date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return '%s' % self.sum
