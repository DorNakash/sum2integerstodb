import Axios from 'axios'

const $axios = Axios.create({
    baseURL: 'http://127.0.0.1:8000/api/',
    headers: {
        'Content-Type': 'application/json'
    }
})

//Example of a cross-cutting concern - client api error-handling
$axios.interceptors.response.use(
    (response) => response,
    (error) => {
        console.error("got error")
        console.error(error)

        throw error;
    });


class SumService {
    static getSum(firstNum,secondNum) {
        return $axios
            .get('sums/sum_two_numbers', {params:{firstNumber:firstNum,secondNumber:secondNum}})

            .then(response => response.data)
    }
}

const service = {
    SumService
}

export default service
