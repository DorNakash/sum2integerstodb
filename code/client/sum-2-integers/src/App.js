import logo from './logo.svg';
import './App.css';
import {useState} from "react";
import service from "./api";

function App() {
  const [firstNum, setFirstNumber] = useState(0);
  const [secondNum, setSecondNumber] = useState(0);
  const [sum, setSum] = useState(0);

  const CalculateSumOnClick = () => {
      service.SumService.getSum(firstNum,secondNum)
          .then(response => {
              setSum(response.sum)
          })
  }

  return (
    <div className="App">
      <input placeholder={'firstNumber'} onChange={event => setFirstNumber(event.target.value)}/>
      <input placeholder={'SecondNumber'} onChange={event => setSecondNumber(event.target.value)}/>
      <button onClick={CalculateSumOnClick}>Click for the sum</button>
      <output>{sum}</output>
    </div>
  );
}

export default App;
